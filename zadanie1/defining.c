//
// Created by emented on 12.11.2022.
//
#include <stdio.h>
#define print_var(x) printf(#x " is %d", x )

int main() {
    int x = 2111;
    const int y = 12;
    print_var(43);
    printf("\n");
    print_var(x);
    printf("\n");
    print_var(y);
    printf("\n");
    return 0;
}