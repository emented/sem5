//
// Created by emented on 12.11.2022.
//
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdlib.h>

void print_int64(int64_t value) {
    printf("%" PRId64 "\n", value);
}

void print_double(double value) {
    printf("%lf" "\n", value);
}

void print_char(char value) {
    printf("%c" "\n", value);
}

void error(const char *s) {
    fprintf(stderr, "%s", s);
    abort();
}

#define print_val(value)                                                    \
    _Generic((value),                                                       \
        int64_t: print_int64(value),                                        \
        double: print_double(value),                                        \
        char: print_char(value),                                            \
        default: error("Unsupported operation"))

#define DEFINE_LIST(type)                                                   \
    struct list_##type {                                                    \
        type value;                                                         \
        struct list_##type* next;                                           \
    };                                                                      \
    struct list_##type* init_list_##type(type key) {                        \
        struct list_##type* list = malloc(sizeof(struct list_##type));      \
        list->value = key;                                                  \
        list->next = NULL;                                                  \
        return list;                                                        \
    };                                                                      \
    bool list_push_##type(struct list_##type* list_pointer, type key){      \
        struct list_##type* main_list_pointer = list_pointer;               \
        struct list_##type* value = init_list_##type(key);                  \
        while(main_list_pointer->next != NULL) {                            \
            main_list_pointer = main_list_pointer->next;                    \
        }                                                                   \
        main_list_pointer->next = value;                                    \
        return true;                                                        \
    };                                                                      \
    void list_print_##type(struct list_##type* list_pointer) {              \
        struct list_##type* main_list_pointer = list_pointer;               \
        while (main_list_pointer != NULL) {                                 \
            print_val(main_list_pointer->value);                            \
            main_list_pointer = main_list_pointer->next;                    \
        };                                                                  \
    };

#define pprint(type, list)                                                  \
    list_print_##type(list)

#define ppush(type, list, value)                                            \
    list_push_##type(list, value)

#define iinit(type, value)                                                  \
    init_list_##type(value)

DEFINE_LIST(int64_t)
DEFINE_LIST(double)
DEFINE_LIST(char)


int main() {
    struct list_int64_t *listInt64 = iinit(int64_t, 25);
    for (int i = 0; i < 10; i++) {
        ppush(int64_t, listInt64, i);
    }

    struct list_double *listDouble = iinit(double, 16.2);
    for (int i = 1; i < 10; i++) {
        ppush(double, listDouble, (double) 773/i);
    }

    struct list_char *listChar = iinit(char, 'M');
    ppush(char, listChar, 'a');
    ppush(char, listChar, 'x');
    ppush(char, listChar, 'i');
    ppush(char, listChar, 'm');
    printf("Integers:\n");
    pprint(int64_t, listInt64);
    printf("Doubles:\n");
    pprint(double, listDouble);
    printf("Chars:\n");
    pprint(char, listChar);

    return 0;
}